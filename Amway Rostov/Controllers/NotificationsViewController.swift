//
//  NotificationsViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 10/11/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class NotificationsViewController: UIPageViewController {
    var myDelegate: CoursesViewController!
    var notificationVCs = [NotificationViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        let hours = Calendar.current.component(.hour, from: Date())
        
        let data = UserDefaults.standard.dictionary(forKey: "TimeTable")!
        
        //print(data)
        
        let date = data["date"] as! [String: String]
        let notificationDateStr = "\(date["year"]!)-\(date["month"]!)-\(date["day"]!)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let startDate = dateFormatter.date(from: notificationDateStr)!
        
        let daysInterval = Calendar.current.dateComponents([.day], from: startDate, to: Date()).day!
        let week = daysInterval / 7 + 1
        let day = daysInterval - (week - 1) * 7 + 1
        
        //print("\(week) \(day)")
        
        let table = data["table"] as! [String: AnyObject]
        let tWeek = table["\(week)"] as! [String:AnyObject]
        let tDayNotifications = tWeek["\(day)"] as! [String: AnyObject]
        let sorted = tDayNotifications.sorted(by: { (obj1, obj2) -> Bool in return obj1.key < obj2.key})
        
        for i in sorted.indices {
            dateFormatter.dateFormat = "HH:ss"
            let date = dateFormatter.date(from: sorted[i].key)!
            let time = Calendar.current.component(.hour, from: date)
            if hours >= time {
                notificationVCs.append(getNotificationVCfromTime(notification: tDayNotifications[sorted[i].key] as! [String: AnyObject]))
            }
        }
        
        self.setViewControllers([notificationVCs.last!], direction: .forward, animated: true, completion: nil)
    }
    
    fileprivate func getNotificationVCfromTime(notification: [String: AnyObject]) -> NotificationViewController {
        var userInfo = [String: Data]()
        userInfo["notification"] = try! JSONEncoder().encode(Notification(link: notification["link"] as! String, text: notification["text"] as! String, type: notification["type"] as! String))
        if let button1 = notification["button1"] as? [String: AnyObject] {
            userInfo["button1"] = try! JSONEncoder().encode(Notification(link: button1["link"] as! String, text: button1["text"] as! String, type: button1["type"] as! String))
        }
        if let button2 = notification["button2"] as? [String: AnyObject] {
            userInfo["button2"] = try! JSONEncoder().encode(Notification(link: button2["link"] as! String, text: button2["text"] as! String, type: button2["type"] as! String))
        }
        let VC = getNotificationVC()
        VC.userInfo = userInfo
        VC.id = notification["id"] as? String
        return VC
    }
    
    fileprivate func getEmptyNotificationVC() -> NotificationViewController {
        return NotificationViewController()
    }
}

extension NotificationsViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let index = notificationVCs.firstIndex(of: viewController as! NotificationViewController)!
        if index > 0 {
            return notificationVCs[index - 1]
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let index = notificationVCs.firstIndex(of: viewController as! NotificationViewController)!
        if index < notificationVCs.count - 1 {
            return notificationVCs[index + 1]
        }
        return nil
    }
}

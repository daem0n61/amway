//
//  EditViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 25/09/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {
    @IBOutlet var textFields: [UITextField]!
    let picker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createPickerAndToolbar()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != nil {
            view.endEditing(true)
        }
        super.touchesBegan(touches, with: event)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let session = getAppDelegate().session!
        DispatchQueue.global(qos: .userInitiated).async {
            let answer = API.shared.getUser(session: session)
            do {
                let parsedUser = try JSONDecoder().decode(User.self, from: answer)
                DispatchQueue.main.async {
                    self.textFields[0].text = parsedUser.surname
                    self.textFields[1].text = parsedUser.name
                    self.textFields[2].text = parsedUser.patr
                    self.textFields[3].text = parsedUser.bd
                    self.textFields[4].text = parsedUser.phone
                    self.textFields[5].text = parsedUser.address
                    self.textFields[6].text = parsedUser.id_amway
                    self.textFields[7].text = parsedUser.id_sponsor
                }
            } catch {
                presentAllert(VC: self, message: "Соединение с сервером отсутствует")
                return
            }
        }
    }
    
    fileprivate func createPickerAndToolbar() {
        picker.datePickerMode = .date
        picker.minimumDate = Date().addingTimeInterval(-60 * 60 * 24 * 365 * 80)
        picker.maximumDate = Date()
        textFields[3].inputView = picker
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let done = UIBarButtonItem(title: "Готово", style: .done, target: nil, action: #selector(doneTapped))
        toolbar.setItems([done], animated: false)
        textFields[3].inputAccessoryView = toolbar
    }
    
    @objc fileprivate func doneTapped() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        textFields[3].text = dateFormatter.string(from: picker.date)
        view.endEditing(true)
    }
    
    @IBAction func okEditPressed(_ sender: Any) {
        if (textFields[0].text! == "" || textFields[1].text! == "") {
            presentAllert(VC: self, message: "Заполните пустые поля (кроме необязательных)")
        }
        
        let answer = API.shared.edit(session: getAppDelegate().session ,name: textFields[1].text!, surname: textFields[0].text!, patr: textFields[2].text!, bd: textFields[3].text!, phone:  textFields[4].text!, address: textFields[5].text!, amwayID: textFields[6].text!, sponsorID: textFields[7].text!)
        do {
            let parsedAnswer = try JSONDecoder().decode(Answer.self, from: answer)
            switch parsedAnswer.answer {
            case "success":
                presentAllert(VC: self, message: "Изменения сохранены")
            case "error":
                presentAllert(VC: self, message: "Заполните пустые поля (кроме AmwayID и SponsorID)")
            case "connection_error":
                presentAllert(VC: self, message: "Соединение с сервером отсутствует")
            default:
                break
            }
        } catch {
            presentAllert(VC: self, message: "Соединение с сервером отсутствует")
            return
        }
    }
    
    @IBAction func changePasswordButtonPressed(_ sender: Any) {
        let alertVC = UIAlertController(title: "Восстановление пароля", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        let restoreAlert = UIAlertAction(title: "Восстановить", style: .default) { (action) in
            guard let pass1 = alertVC.textFields?[0].text else {return}
            guard let pass2 = alertVC.textFields?[1].text else {return}
            if (pass1 == "" || pass2 == "") {
                presentAllert(VC: self, message: "Заполните пустые поля")
                return
            }
            if pass1 != pass2 {
                presentAllert(VC: self, message: "Пароли не совпадают")
                return
            }
            
            API.shared.changePassword(password: pass1, session: getAppDelegate().session)
            let loginDate = UserDefaults.standard.stringArray(forKey: "LoginData")
            UserDefaults.standard.set([loginDate![0], pass1], forKey: "LoginData")
            presentAllert(VC: self, message: "Пароль успешно изменен")
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Введите новый пароль"
            textField.isSecureTextEntry = true
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Повторите новый пароль"
            textField.isSecureTextEntry = true
        }
        alertVC.addAction(cancelAction)
        alertVC.addAction(restoreAlert)
        self.present(alertVC, animated: true, completion: nil)
    }
}

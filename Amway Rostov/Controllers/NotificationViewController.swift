//
//  NotificationViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 29/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit
import WebKit

class NotificationViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var sendMailButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var dislikeButton: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    var userInfo: [String: Data]!
    var id: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateRating()
        
        let session = getAppDelegate().session!
        DispatchQueue.global(qos: .utility).async {
            _ = API.shared.viewPush(session: session, push_id: self.id)
        }
        
        let notification = try! JSONDecoder().decode(Notification.self, from: userInfo["notification"]!)
        
        self.title = notification.text
        
        if notification.type == "pdf" {
            webView.isHidden = true
            sendMailButton.isHidden = false
        } else {
            guard let url = URL(string: "https://www.youtube.com/embed/\(notification.link)?playsinline=1&rel=0&disablekb=1") else {return}
            webView.load(URLRequest(url: url))
        }
        
        let button1 = try! JSONDecoder().decode(Notification.self, from: userInfo["button1"]!)
        if button1.text == "none" {
            self.button1.isHidden = true
        } else {
            self.button1.setTitle(button1.text, for: .normal)
        }
        
        let button2 = try! JSONDecoder().decode(Notification.self, from: userInfo["button2"]!)
        if button2.text == "none" {
            self.button2.isHidden = true
        } else {
            self.button2.setTitle(button2.text, for: .normal)
        }
    }
    
    fileprivate func updateRating() {
        DispatchQueue.main.async {
            let session = getAppDelegate().session!
            DispatchQueue.global(qos: .userInteractive).async {
                let data = API.shared.rating(session: session, push_id: self.id)
                let parsed = try! JSONDecoder().decode(Answer.self, from: data)
                DispatchQueue.main.async {
                    switch parsed.answer {
                        case "0":
                            self.likeButton.setImage(UIImage(named: "Like"), for: .normal)
                            self.dislikeButton.setImage(UIImage(named: "DislikeFilled"), for: .normal)
                        case "1":
                            self.likeButton.setImage(UIImage(named: "LikeFilled"), for: .normal)
                            self.dislikeButton.setImage(UIImage(named: "Dislike"), for: .normal)
                        default:
                            break
                    }
                }
            }
        }
    }
    
    @IBAction func sendMailButtonPressed(_ sender: Any) {
        let session = getAppDelegate().session!
        let notification = try! JSONDecoder().decode(Notification.self, from: userInfo["notification"]!)
        DispatchQueue.global(qos: .userInteractive).async {
            API.shared.sendMail(session: session, link: notification.link)
            presentAllert(VC: self, message: "Содержимое отправлено Вам на почту")
        }
    }
    
    func buttonAction(notification: Notification) {
        switch notification.type {
        case "site":
            UIApplication.shared.open(URL(string: notification.link)!)
            break
        default:
            let session = getAppDelegate().session!
            DispatchQueue.global(qos: .userInteractive).async {
                let data = API.shared.sendMail(session: session, link: notification.link)
                presentAllert(VC: self, message: "Содержимое отправлено Вам на почту")
            }
        }
    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        let session = getAppDelegate().session!
        DispatchQueue.global(qos: .userInitiated).async {
            API.shared.like(session: session, push_id: self.id)
            //let data = API.shared.rating(session: session, push_id: self.id!)
            //printJSON(data: data)
            self.updateRating()
        }
        
    }
    
    @IBAction func dislikeButtonPressed(_ sender: Any) {
        let session = getAppDelegate().session!
        DispatchQueue.global(qos: .userInitiated).async {
            API.shared.dislike(session: session, push_id: self.id)
            //let data = API.shared.rating(session: session, push_id: self.id!)
            //printJSON(data: data)
            self.updateRating()
        }
    }
    
    
    
    @IBAction func button1Pressed(_ sender: Any) {
        let notification = try! JSONDecoder().decode(Notification.self, from: userInfo["button1"]!)
        buttonAction(notification: notification)
    }
    
    @IBAction func button2Pressed(_ sender: Any) {
        let notification = try! JSONDecoder().decode(Notification.self, from: userInfo["button2"]!)
        buttonAction(notification: notification)
    }
}

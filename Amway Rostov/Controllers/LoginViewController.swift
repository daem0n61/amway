//
//  LoginViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 22.09.2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var loginText: UITextField!
    @IBOutlet weak var passText: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != nil {
            view.endEditing(true)
        }
        super.touchesBegan(touches, with: event)
    }
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        let login = loginText.text!
        let password = passText.text!
        if (login == "" || password == "") {
            presentAllert(VC: self, message: "Заполните пустые поля")
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let answer = API.shared.login(login: login, password: password)
            printJSON(data: answer)
            let parsedAnswer = try! JSONDecoder().decode(Answer.self, from: answer)
            
            DispatchQueue.main.async {
                switch parsedAnswer.answer {
                case "success":
                    UserDefaults.standard.set([login, password], forKey: "LoginData")
                    getAppDelegate().session = parsedAnswer.session_id
                    self.performSegue(withIdentifier: "ShowTabBar", sender: self)
                case "error":
                    presentAllert(VC: self, message: "Ошибка. Попробуйте еще раз")
                case "wrong_auth":
                    presentAllert(VC: self, message: "Неверный логин или пароль")
                case "connection_error":
                    presentAllert(VC: self, message: "Соединение с сервером отсутствует")
                default:
                    break;
                }
            }
        }
    }
    
    @IBAction func forgotButtonPressed(_ sender: Any) {
        let alertVC = UIAlertController(title: "Восстановление пароля", message: nil, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        let restoreAlert = UIAlertAction(title: "Восстановить", style: .default) { (action) in
            guard let email = alertVC.textFields?[0].text else {
                presentAllert(VC: self, message: "Что-то пошло не так, попробуйте еще раз")
                return
            }
            
            if email == "" {
                presentAllert(VC: self, message: "Введена пустая почта")
                return
            }
            
            API.shared.forgotPassword(version: "version2", email: email)
            presentAllert(VC: self, message: "Пароль отправлен Вам на почту")
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Введите свою почту"
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        alertVC.addAction(cancelAction)
        alertVC.addAction(restoreAlert)
        self.present(alertVC, animated: true, completion: nil)
    }
}

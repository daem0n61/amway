//
//  CatalogItem.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 10/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import Foundation

struct User: Decodable {
    let answer, name, surname, patr, bd, phone, email, address, id_amway, id_sponsor, rights: String
}

struct Answer: Decodable {
    let answer: String
    let session_id, version: String?
}

struct Time: Decodable {
    let d, h, m: Int
}

struct CatalogItem {
    var title, url: String
}

struct Course: Decodable {
    let id: Int
    let name: String
}

struct Courses: Decodable {
    let answer: String
    let list: [Course]?
}

struct SubsCourse: Decodable {
    let id: Int
}

struct SubsCourses: Decodable {
    let answer: String
    let list: [SubsCourse]?
}

struct Notification: Codable {
    let link, text, type: String
}

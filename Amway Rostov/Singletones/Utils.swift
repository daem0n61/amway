//
//  Utils.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 29/09/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import Foundation
import UIKit

func presentAllert(VC: UIViewController, message: String) {
    let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertVC.addAction(alertAction)
    VC.present(alertVC, animated: true, completion: nil)
}

func getLoginNavigationVC() -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginNavVC")
}

func getTabBarVC(index: Int = 0) -> UITabBarController {
    let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
    VC.selectedIndex = index
    if index == 3 {
        let navVC = VC.selectedViewController as! UINavigationController
        navVC.pushViewController(getRegionVC(), animated: true)
    }
    return VC
}

func getRegionVC() -> UIViewController {
    let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegionVC")
    return VC
}

func getNotificationsVC() -> NotificationsViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsViewController
}

func getNotificationVC() -> NotificationViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationVC") as! NotificationViewController
}

func getAppDelegate() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

func printJSON(data: Data) {
    print(try! JSONSerialization.jsonObject(with: data, options: []))
}

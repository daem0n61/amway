//
//  UserNotificationManager.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 24/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UserNotifications

class NotificationManager: NSObject {
    static let shared = NotificationManager()
    private let notificationCenter = UNUserNotificationCenter.current()
    
    func registration() {
        notificationCenter.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                self.notificationCenter.requestAuthorization(options: [.alert, .sound]) { (granted, error) in }
            }
        }
        UNUserNotificationCenter.current().delegate = self
    }
    
    func createNotification(identifier: String, notification: Notification, date: Date) {
        let content = UNMutableNotificationContent()
        content.title = notification.text
        content.sound = UNNotificationSound.default
        
        //let date = Date(timeIntervalSinceNow: 3)
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        print(date)
        notificationCenter.add(request)
    }
    
    func clear() {
        notificationCenter.removeAllDeliveredNotifications()
        notificationCenter.removeAllPendingNotificationRequests()
    }
}

extension NotificationManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let VC = getTabBarVC(index: 3)
        getAppDelegate().window?.rootViewController = VC
        
        completionHandler()
    }
}

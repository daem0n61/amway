//
//  CabinetViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 26/09/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class CabinetViewController: UIViewController {
    @IBOutlet var textViews: [UITextView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationManager.shared.registration()
        
        //NotificationManager.shared.clear()
        //TimeTable.shared.clear()
        TimeTable.shared.refresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (getAppDelegate().session == nil) {
            presentAllert(VC: self, message: "Повторите вход в аккаунт")
            return
        }
        
        let session = getAppDelegate().session!
        
        DispatchQueue.global(qos: .utility).async {
            do {
                let answer = API.shared.getUser(session: session)
                let parsedUser = try JSONDecoder().decode(User.self, from: answer)
                DispatchQueue.main.async {
                    switch parsedUser.answer {
                    case "success":
                        self.textViews[0].text = "\(parsedUser.surname) \(parsedUser.name) \(parsedUser.patr)"
                        self.textViews[1].text = "Amway ID: \(parsedUser.id_amway)"
                        self.textViews[2].text = "Sponsor ID: \(parsedUser.id_sponsor)"
                        self.textViews[3].text = parsedUser.email
                        self.textViews[4].text = parsedUser.phone
                    case "error":
                        presentAllert(VC: self, message: "Повторите вход в аккаунт")
                    case "connection_error":
                        presentAllert(VC: self, message: "Соединение с сервером отсутствует")
                    default:
                        break
                    }
                }
            } catch {
                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
            }
        }
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "LoginData")
        UserDefaults.standard.removeObject(forKey: "TimeTable")
        UserDefaults.standard.removeObject(forKey: "TableVersion")
        getAppDelegate().window?.rootViewController = getLoginNavigationVC()
    }
}

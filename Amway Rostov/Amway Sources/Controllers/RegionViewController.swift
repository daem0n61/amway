//
//  RegionViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 23/11/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class RegionViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let VC = segue.destination as? NotificationsViewController
        print(VC)
        let hour = Calendar.current.component(.hour, from: Date())
        if hour >= 8 { VC?.firstVC = 0}
        if hour >= 14 { VC?.firstVC = 1}
        if hour >= 19 { VC?.firstVC = 2}
    }
}

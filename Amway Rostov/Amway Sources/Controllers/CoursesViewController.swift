//
//  CoursesViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 24/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class CoursesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let refresh = UIRefreshControl()
    var coursesList = [Course]()
    var subsList = [SubsCourse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*print("|===========================================================|")
        print(getAppDelegate().window?.rootViewController)
        print(self.tabBarController)
        print(self.navigationController)
        print(self)*/
        
        refresh.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        tableView.addSubview(refresh)
        
        refreshHandler()
    }
    
    @objc func refreshHandler() {
        let session = getAppDelegate().session!
        
        DispatchQueue.global(qos: .userInitiated).async {
            let answer = API.shared.getUser(session: session)
            do {
                let parsedUser = try JSONDecoder().decode(User.self, from: answer)
                switch parsedUser.answer {
                case "success":
                    if parsedUser.rights == "1" {
                        DispatchQueue.main.async {
                            presentAllert(VC: self, message: "Чтобы просматривать доступные курсы, нужно заполнить поля Amway id и Sponsor id в личной информации")
                            self.refresh.endRefreshing()
                        }
                        return
                    }
                default:
                    break
                }
            } catch {
                return
            }
            
            let answer1 = API.shared.getCoursesList(session: session)
            do {
                let parsedAnswer = try JSONDecoder().decode(Courses.self, from: answer1)
                switch parsedAnswer.answer {
                case "success":
                    self.coursesList = parsedAnswer.list!
                default:
                    break
                }
            } catch {
                //presentAllert(VC: self, message: "Соединение с сервером отсутствует 1")
                return
            }
            
            let answer2 = API.shared.getSubs(session: session)
            do {
                let parsedAnswer2 = try JSONDecoder().decode(SubsCourses.self, from: answer2)
                switch parsedAnswer2.answer {
                case "success":
                    self.subsList = parsedAnswer2.list!
                case "empty":
                    self.subsList.removeAll()
                default:
                    break
                }
            } catch {
                //presentAllert(VC: self, message: "Соединение с сервером отсутствует 2")
                return
            }
            
            DispatchQueue.main.async {
                self.refresh.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    func presentErrorAlert() {
        presentAllert(VC: self, message: "На текущий момент ничего нет. Ждите новых уроков.")
    }
}

extension CoursesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coursesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CourseCell", for: indexPath) as! CourseTableViewCell
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = coursesList[indexPath.row].name
        cell.choiseSwitch.tag = indexPath.row
        cell.choiseSwitch.addTarget(self, action: #selector(choiceSwitched(_:)), for: .valueChanged)
        for course in subsList {
            if course.id == coursesList[indexPath.row].id {
                cell.choiseSwitch.isOn = true
                cell.choiseSwitch.isEnabled = false
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let data = UserDefaults.standard.dictionary(forKey: "TimeTable"), let date = data["date"] as? [String: String] {
            let notificationDateStr = "\(date["year"]!)-\(date["month"]!)-\(date["day"]!)"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let startDate = dateFormatter.date(from: notificationDateStr)!
            
            let daysInterval = Calendar.current.dateComponents([.day], from: startDate, to: Date()).day!
            let week = daysInterval / 7 + 1
            let day = daysInterval - (week - 1) * 7 + 1
            
            if let table = data["table"] as? [String: AnyObject], let tWeek = table["\(week)"] as? [String:AnyObject], let _ = tWeek["\(day)"] as? [String: AnyObject] {
                let VC = getRegionVC()
                VC.title = coursesList[0].name
                self.navigationController?.pushViewController(VC, animated: true)
            } else {
                presentAllert(VC: self, message: "На текущий момент ничего нет. Ждите новых уроков.")
            }
        } else {
            presentAllert(VC: self, message: "На текущий момент ничего нет. Ждите новых уроков.")
        }
    }
    
    @objc func choiceSwitched(_ sender: UISwitch) {
        if (sender.isOn) {
            let session = getAppDelegate().session!
            DispatchQueue.global(qos: .utility).async {
                API.shared.subscribeCourse(session: session, course: 1)
                TimeTable.shared.refresh()
                DispatchQueue.main.async {
                    self.refreshHandler()
                }
            }
            presentAllert(VC: self, message: "Вы подписались на курс \"\(coursesList[sender.tag].name)\"")
        } else {
            presentAllert(VC: self, message: "Вы отписались от курса \"\(coursesList[sender.tag].name)\"")
        }
    }
}

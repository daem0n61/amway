//
//  PdfViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 10/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit
import WebKit

class PdfViewController: UIViewController {
    @IBOutlet weak var pdfView: WKWebView!
    
    var item: CatalogItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = item.title
        if let url = Bundle.main.url(forResource: item.url, withExtension: "pdf") {
            pdfView.load(URLRequest(url: url))
        } else {
            print("onlinepdf")
            //let session = URLSession(configuration: .default)
            //let downloadTask = session.downloadTask(with: URL(string: item.url)!)
            pdfView.load(URLRequest(url: URL(string: item.url)!))
        }
    }
}

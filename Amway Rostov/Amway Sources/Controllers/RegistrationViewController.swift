//
//  RegistrationViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 23.09.2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    @IBOutlet var textFields: [UITextField]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != nil {
            view.endEditing(true)
        }
        super.touchesBegan(touches, with: event)
    }
    
    @IBAction func okBtnPressed(_ sender: Any) {
        if (textFields[0].text! == "" || textFields[1].text! == "" || textFields[2].text! == "") {
            presentAllert(VC: self, message: "Заполните пустые поля")
            return
        }
        
        let data = API.shared.registration(version: "version2", email: textFields[0].text!, name: textFields[1].text!, surname: textFields[2].text!)
        do {
            let parsedAnswer = try JSONDecoder().decode(Answer.self, from: data)
            switch parsedAnswer.answer {
            case "success":
                presentAllert(VC: self, message: "Пароль отправлен Вам на почту")
            case "error":
                presentAllert(VC: self, message: "Заполните пустые поля")
            case "email_busy":
                presentAllert(VC: self, message: "Логин уже занят")
            case "connection_error":
                presentAllert(VC: self, message: "Соединение с сервером отсутствует")
            default:
                break;
            }
        } catch {
            presentAllert(VC: self, message: "Соединение с сервером отсутствует")
            return
        }
    }
}

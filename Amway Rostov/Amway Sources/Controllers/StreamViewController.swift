//
//  StreamViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 19.09.2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit
import WebKit

class StreamViewController: UIViewController {
    @IBOutlet weak var videoView: WKWebView!
    @IBOutlet weak var timeToStream: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videoView.scrollView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.global(qos: .utility).async {
            let time = API.shared.getTime()
            do {
                let parsedTime = try JSONDecoder().decode(Time.self, from: time)
                DispatchQueue.main.async {
                    if (parsedTime.d == 0 && parsedTime.h == 0 && parsedTime.m == 0) {
                        self.timeToStream.text = "Прямой эфир"
                    }
                    else {
                        self.timeToStream.text = "До начала трансляции "
                        if parsedTime.d > 0 {
                            self.timeToStream.text += "\(parsedTime.d)д "
                        }
                        if parsedTime.h > 0 {
                            self.timeToStream.text += "\(parsedTime.d)ч "
                        }
                        if parsedTime.m > 0 {
                            self.timeToStream.text += "\(parsedTime.d)м"
                        }
                    }
                }
            } catch {
                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
            }
        }
        
        DispatchQueue.global(qos: .utility).async {
            let answer = API.shared.getVideo()
            do {
                let parsedAnswer = try JSONDecoder().decode(Answer.self, from: answer)
                DispatchQueue.main.async {
                    self.playVideo(code: parsedAnswer.answer)
                }
            } catch {
                presentAllert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
            }
        }
    }
    
    private func playVideo(code: String) {
        guard let url = URL(string: "https://www.youtube.com/embed/\(code)?playsinline=1&rel=0&disablekb=1") else {return}
        videoView.load(URLRequest(url: url))
    }
}

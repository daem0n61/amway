//
//  CatalogViewController.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 10/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class CatalogViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var refresh = UIRefreshControl()
    
    var catalogItems = [CatalogItem(title: "Nutrilite", url: "Nutrilite"),
                        CatalogItem(title: "Beauty", url: "Beauty")]

    override func viewDidLoad() {
        super.viewDidLoad()
        //API.getCatalog(session: AppDelegate.session!)
        //refresh.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        //refresh.tintColor = UIColor.blue
        //tableView.addSubview(refresh)
    }
    
    @objc func refreshHandler() {
        refresh.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension CatalogViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catalogItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatalogCell", for: indexPath)
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = catalogItems[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = catalogItems[indexPath.row]
        let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PdfVC") as! PdfViewController
        VC.item = item
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

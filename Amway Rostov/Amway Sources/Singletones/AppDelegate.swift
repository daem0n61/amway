//
//  AppDelegate.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 19.09.2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var session: String!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        var VC: UIViewController
        if let loginDate = UserDefaults.standard.stringArray(forKey: "LoginData") {
            let answer = API.shared.login(login: loginDate[0], password: loginDate[1])
            do {
                let parsedAnswer = try JSONDecoder().decode(Answer.self, from: answer)
                switch parsedAnswer.answer {
                case "success":
                    session = parsedAnswer.session_id
                    VC = getTabBarVC()
                default:
                    VC = getLoginNavigationVC()
                }
            } catch {
                VC = getLoginNavigationVC()
            }
        }
        else {
            VC = getLoginNavigationVC()
        }
        window?.rootViewController = VC
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

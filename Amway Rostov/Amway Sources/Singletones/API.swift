//
//  HttpUtility.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 22.09.2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import Foundation

class API {
    static let shared = API()
    private var data: Data!
    private let baseURL = "http://www.abcamway.ru/api"
    
    func login(login: String, password: String) -> Data {
        let params = "email=\(login)&password=\(password)&version=version2"
        self.configRequest(url: "auth", params: params)
        return data
    }
    
    func registration(version: String, email: String, name: String, surname: String) -> Data {
        let params = "version=version2&name=\(name)&surname=\(surname)&email=\(email)"
        configRequest(url: "registration", params: params)
        return data
    }
    
    func forgotPassword(version: String, email: String) -> Data {
        let params = "version=version2&email=\(email)"
        configRequest(url: "forgotPassword", params: params)
        return data
    }
    
    func getUser(session: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)"
        configRequest(url: "getUser", params: params)
        return data
    }
    
    func edit(session: String, name: String, surname: String, patr: String?, bd: String?, phone: String?, address: String?, amwayID: String?, sponsorID: String?) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&name=\(name)&surname=\(surname)&patr=\(patr ?? " ")&bd=\(bd ?? " ")&phone=\(phone ?? " ")&address=\(address ?? " ")&id_amway=\(amwayID ?? " ")&id_sponsor=\(sponsorID ?? " ")"
        configRequest(url: "editUser", params: params)
        return data
    }
    
    func changePassword(password: String, session: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&pass=\(password)"
        configRequest(url: "changePassword", params: params)
        return data
    }
    
    func getTime() -> Data {
        configRequest(url: "getTime", params: "version=version2")
        return data
    }
    
    func getVideo() -> Data {
        configRequest(url: "getVideo", params: "version=version2")
        return data
    }
    
    func getCatalog(session: String) -> Data {
        //let params = "PHPSESSID=\(session)"
        configRequest(url: "getCatalog", params: "version=version2")
        return data
    }
    
    func getTimeTable(session: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&id=1"
        configRequest(url: "getTimeTable", params: params)
        return data
    }
    
    func getVersion(session: String, course: Int) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&id=\(course)"
        configRequest(url: "checkVersion", params: params)
        return data
    }
    
    func subscribeCourse(session: String, course: Int) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&id=\(course)"
        configRequest(url: "subscribeCourse", params: params)
        return data
    }
    
    func getCoursesList(session: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)"
        configRequest(url: "getCoursesList", params: params)
        return data
    }
    
    func getSubs(session: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)"
        configRequest(url: "getSubs", params: params)
        return data
    }
    
    func sendMail(session: String, link: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&link=\(link)"
        configRequest(url: "sendMail", params: params)
        return data
    }
    
    func like(session: String, push_id: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&PUSH_ID=\(push_id)"
        configRequest(url: "like", params: params)
        return data
    }
    
    func dislike(session: String, push_id: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&PUSH_ID=\(push_id)"
        configRequest(url: "dislike", params: params)
        return data
    }
    
    func rating(session: String, push_id: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&PUSH_ID=\(push_id)"
        configRequest(url: "rating", params: params)
        return data
    }
    
    func viewPush(session: String, push_id: String) -> Data {
        let params = "version=version2&PHPSESSID=\(session)&PUSH_ID=\(push_id)"
        configRequest(url: "viewPush", params: params)
        return data
    }
    
    fileprivate func configRequest(url: String, params: String) {
        guard let url = URL(string: "\(baseURL)/\(url)") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = params.data(using: .utf8)
        //request.addValue("application/json", forHTTPHeaderField: "Content-type")
        //guard let httpBody = try! JSONSerialization.data(withJSONObject: params, options:[]) else {return}
        //request.httpBody = httpBody
        sendRequest(request: request)
    }
    
    fileprivate func sendRequest(request: URLRequest) {
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            //if let response = response {print(response)}
            guard let data = data else {
                semaphore.signal()
                return
            }
            self.data = data
            semaphore.signal()
        }.resume()
        semaphore.wait()
    }
}

//
//  TimeTable.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 27/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import Foundation

class TimeTable {
    static let shared = TimeTable()
    
    func refresh() {
        let session = getAppDelegate().session!
        DispatchQueue.global(qos: .utility).async {
            var newVersion = String()
            do {
                let answer = API.shared.getVersion(session: session, course: 1)
                do {
                    let parsedAnswer = try JSONDecoder().decode(Answer.self, from: answer)
                    switch parsedAnswer.answer {
                    case "success":
                        newVersion = parsedAnswer.version!
                    case "empty_subs":
                        print("empty_subs")
                    default:
                        break
                    }
                } catch {
                    print(error)
                    print("connection_error")
                }
            }
            
            if let oldVersion = UserDefaults.standard.string(forKey: "TableVersion") {
                print("Версия найдена")
                if oldVersion == newVersion {
                    print("Версии совпали")
                    return
                }
                else {
                    print("Версии не совпали")
                }
            }
            
            let data = API.shared.getTimeTable(session: session)
            var parsed: [String: AnyObject]
            do {
                parsed = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            } catch {
                print(error)
                return
            }
            
            UserDefaults.standard.set(newVersion, forKey: "TableVersion")
            UserDefaults.standard.set(parsed, forKey: "TimeTable")
            
            guard let date = parsed["date"] as? [String: String] else {return }
            print(date)
            
            if let table = parsed["table"] as? [String: AnyObject] {
                //print(table)
                //print("|--------------------------------------------------------------------------------------------|")
                for weekIndex in 1...4 /*where weekIndex == 1*/ {
                    if let week = table["\(weekIndex)"] as? [String: AnyObject] {
                        //print("\(weekIndex) \(week)")
                        for dayIndex in 1...7 /*where dayIndex == 3*/ {
                            if let day = week["\(dayIndex)"] as? [String: AnyObject] {
                                //print("\(dayIndex) \(day)")
                                for time in ["08:00", "14:00", "19:00"] /*where time == "19:00"*/ {
                                    if let notification = day["\(time)"] as? [String: AnyObject] {
                                        //print("\(time) \(notification)")
                                        
                                        let notificationDateStr = "\(date["year"]!)-\(date["month"]!)-\(date["day"]!) \(time)"
                                        
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
                                        
                                        let notificationDate = Calendar.current.date(byAdding: .hour, value: 7 * 24 * (weekIndex - 1) + 24 * (dayIndex - 1), to: dateFormatter.date(from: notificationDateStr)!)!
                                        
                                        let curDate = Date()
                                        
                                        if notificationDate < curDate {continue}
                                        
                                        let identifier = "course1-week\(weekIndex)-day\(dayIndex)-time\(time)"
                                        
                                        let notification = Notification(link: notification["link"] as! String, text: notification["text"] as! String, type: notification["type"] as! String)
                                        
                                        //print(identifier)
                                        
                                        NotificationManager.shared.createNotification(identifier: identifier, notification: notification, date: notificationDate)
                                    }
                                    //print("|--------------------------------------------------------------------------------------------|")
                                }
                            }
                            //print("|--------------------------------------------------------------------------------------------|")
                        }
                    }
                    //print("|--------------------------------------------------------------------------------------------|")
                }
            }
        }
    }
    
    func clear() {
        UserDefaults.standard.removeObject(forKey: "TableVersion")
        UserDefaults.standard.removeObject(forKey: "TimeTable")
    }
}

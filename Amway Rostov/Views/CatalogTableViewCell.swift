//
//  CatalogTableViewCell.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 10/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class CatalogTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UITextView!
    
    var catalogItem: CatalogItem? {
        didSet {
            title.text = catalogItem?.title
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

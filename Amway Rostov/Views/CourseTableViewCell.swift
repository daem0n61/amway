//
//  CourseTableViewCell.swift
//  Amway Rostov
//
//  Created by Дмитрий Жданов on 24/10/2018.
//  Copyright © 2018 T-REX studio. All rights reserved.
//

import UIKit

class CourseTableViewCell: UITableViewCell {
    @IBOutlet weak var choiseSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
